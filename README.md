[![coverage report](https://gitlab.com/gitlab-cog/gitlab/badges/master/coverage.svg)](https://gitlab.com/gitlab-cog/gitlab/commits/master)

# GitLab Bundle for Cog

This bundle provides commands for interactions with GitLab.
Permissions are used to control which users are authorized to run each command.

To view more information about the bundle and the documentation for each
command use Cog's help command:

For bundle information:

```
help gitlab
```

For specific command information:

```
help gitlab:broadcast
```

## Testing locally

Run the command like this
```
COG_COMMAND="user-find" COG_ARGV_0=<first arg> COG_ARGC=1 GITLAB_TOKEN=<token> ./cog-command
```

## Configuration

* GITLAB_URL - Required URL of your GitLab instance
* GITLAB_TOKEN - Required admin access token used to authenticate with GitLab API

You can set these environment variables with Cog's dynamic config feature:

```
echo 'GITLAB_URL: "https://gitlab.com"' >> config.yaml
echo 'GITLAB_TOKEN: "AKJAJ4OZ5KYFVRVKZRWM"' >> config.yaml
cogctl dynamic-config create gitlab config.yaml
```

When creating the access token for use with the following environment variables,
make sure the GitLab user that owns the token has admin rights.

## Development

***IMPORTANT***
* You need to have Docker installed to build the container images
* You need to be authenticated (`docker login`) to hub.docker.com with an account that can publish to `gitlab`.
 * Check the `Docker Hub` account under the DevOps Vault.

### Cut a new release (build, push, and tag on GitLab):

Ensure that you have modified the version numbers in `config.yaml` for both the Cog Bundle version, and the Docker tag version.
The version that you have specified will also be used to create a git tag for development tracking.

```
VERSION=0.0.1 make release
```

### Build a new image:

Ensure that you have modified the version numbers in `config.yaml` for both the Cog Bundle version, and the Docker tag version.

Run the following command inside of the source directory, where `VERSION` is what you established in the `docker.yaml` file.
```
VERSION=0.0.1 make build
```

### Push a new image to DockerHub:

Ensure that you have modified the version numbers in `config.yaml` for both the Cog Bundle version, and the Docker tag version.

```
VERSION=0.0.1 make push
```

### Deploy to Marvin

* SSH into `cog.gitlap.com` and sudo up
* Inside root's home directory, start an interactive session with the cog docker container: `docker exec -it $(docker-compose ps -q cog) bash`
* `cd ~/gitlab-cog`
* `curl -O https://gitlab.com/gitlab-cog/gitlab/raw/master/config.yaml`
* `cogctl bundle install config.yaml`
* exit out of docker and `cog.gitlap.com`
* Then in either `#infrastructure` or a private conversation with Marvin issue the following commands:
 * `!bundle versions gitlab` to see the installed versions and the current running.
 * `!bundle disable gitlab` to disable the current version from service.
 * `!bundle enable gitlab 0.0.9` gitlab should be follwed by the version number that you tagged your release with.
