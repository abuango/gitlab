require_relative "client"

module GitLab
  module API
    Project = Struct.new(:id, :public, :default_branch, :name, :name_with_namespace,
                         :path, :path_with_namespace, :web_url, :created_at,
                         :last_activity_at, :archived, :star_count, :forks_count,
                         :repository_storage, :description) do
      def exists?
        true
      end

      def to_a
        [to_h]
      end
    end

    class NullProject < Project
      def exists?
        false
      end

      def to_a
        []
      end
    end

    class ProjectClient
      def initialize(client = Client.new)
        @client = client
      end

      def find(project_path)
        project_required!(project_path)
        response = @client.get("/api/v4/projects/#{CGI.escape(project_path)}")
        case response.code.to_i
        when 200
          projects = JSON.parse(response.body)
          if projects.empty?
            NullProject.new(project_path)
          else
            Project.from_hash(projects)
          end
        when 404
          NullProject.new(project_path)
        else
          fail "Failed to get project #{project_path}: #{response.code} - #{response.message}"
        end
      end

      private

      def project_required!(project_path)
        fail("A project is required") if project_path.nil? || project_path.empty?
      end
    end
  end
end
