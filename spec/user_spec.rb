require "spec_helper"
require "api/user"

module GitLab
  module API
    describe UserClient do
      context "using a stubbed server" do
        let(:user) {
          {
            id: 1,
            username: "myuser",
            name: "A guy",
            email: "the@email.com",
            state: "active",
            created_at: "yesterday",
            two_factor_enabled: "false",
            current_sign_in_at: "just a minute ago",
            last_sign_in_at: "this morning"
          }
        }

        it "gets a user by username" do
          req = stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda {|request| { status: 200, body: [user].to_json } }

          found_user = subject.find("myuser")
          expect(found_user.exists?).to be_truthy
          expect(found_user.to_a.size).to eq(1)
          expect(found_user.to_h).to eq(user)

          remove_request_stub(req)
        end

        it "gets and empty user when it doesn't exists" do
          req = stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 404 )

          expect(subject.find("myuser").exists?).to be_falsey

          remove_request_stub(req)
        end

        it "gets and empty user when the content is empty" do
          req = stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 200, body: [].to_json )

          expect(subject.find("myuser").to_a).to be_empty

          remove_request_stub(req)
        end

        it "fails with an error when no username is provided" do
          expect{subject.find("")}.to raise_error(/A username is required/)
        end

        it "fails with an error when a nil username is provided" do
          expect{subject.find(nil)}.to raise_error(/A username is required/)
        end

        it "fails with an error when the server errs out" do
          req = stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 500 )

          expect{subject.find("myuser")}.to raise_error(
            /Failed to get user myuser: 500/
          )

          remove_request_stub(req)
        end

        it "can block a user" do
          sentinel = {state: user[:state]}
          reqs = [
            (stub_request(:get, /api\/v4\/users\?username=.*/).
             with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
             to_return lambda { |request| { status: 200, body: [user].to_json } }),
            (stub_request(:put, /api\/v4\/users\/1\/block/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda { |request|
              sentinel[:state] = "blocked";
              { status: 200 }
          })]

          subject.block("myuser")
          expect(sentinel[:state]).to eq("blocked")

          reqs.each { |req| remove_request_stub(req) }
        end

        it "can unblock a user" do
          sentinel = {state: "blocked"}
          reqs = [
            (stub_request(:get, /api\/v4\/users\?username=.*/).
             with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
             to_return lambda { |request| { status: 200, body: [user].to_json } }),
            (stub_request(:put, /api\/v4\/users\/1\/unblock/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda { |request|
              sentinel[:state] = user[:state];
              { status: 200 }
          })]

          subject.unblock("myuser")
          expect(sentinel[:state]).to eq(user[:state])

          reqs.each { |req| remove_request_stub(req) }
        end

        it "can update a user's email" do
          reqs = [
            (stub_request(:get, /api\/v4\/users\?username=.*/).
             with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
             to_return lambda { |request| { status: 200, body: [user].to_json } }),
          (stub_request(:put, /api\/v4\/users\/1\?email=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda {|request| { status: 200, body: [user].to_json } }
          )]

          # On success, a confirmation email is sent and the user object is
          # returned with the OLD email.  It updates after confirmation.
          subject.update_email("myuser", "new@email.com")
          found_user = subject.find("myuser")
          expect(found_user.to_h[:email]).to eq("the@email.com")

          reqs.each { |req| remove_request_stub(req) }
        end
      end
    end
  end
end
